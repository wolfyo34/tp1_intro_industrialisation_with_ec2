  resource "aws_key_pair" "deployer" {
    key_name   = "deployer-key"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDEaGdfQanXju6QAPWQv55BnO1R+GqcUXvOiuG0OQTWQK2ZFXlIH7HTOg+gTsEn0XWdUplqiPcjoNWLFPbf0a2ojDrd4CY3QlS/tZRm/BV2i8TnWekN0DZ8rGR++dI3uNCQpDR+op/2ezK2OKuNifW8/vKi87Q7JPb5raceEwSyxhMrHCqwtaviGrLKtMiqrghwauE0g+MfXNzFxQd7I923krKuU/xbcLt/1Mk9P+WZh0ZTUmvGXvcvJO61k0PHqmMLl9CpO643ERUAr8fzVrETUDIIHVyVs9CeFyOg+vFlZsSrRkDJwk45xxvcv8bFp8feOELkL5kH3c1iZhhBq390r8ZtBuZ97Uia04tXZ5WQwBr25xG5NeUSG4c/5EpxLlBh4Jl9wQRJtktf0yqB054bsox6Ru6320CteVYSHZRirPLF5sbS/qUQrRnJb/FBSbLJSBUVZF9MggRjbIJeyHCEValLUm4OhE9V/zYyXI1/wQ5AfiUYGVZXP3rVVJJHhVM= yoannmartinoli@MacBook-Pro-de-Yoann-Martinoli.local"
  }
  
  resource "aws_default_vpc" "default" {
  tags = {
  Name = "Default VPC"
  }
  }
  
  resource "aws_vpc" "mainvpc" {
  cidr_block = "10.1.0.0/16"
  }

  resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.mainvpc.id
  
  ingress {
  protocol  = -1
  self      = true
  from_port = 0
  to_port   = 0
  }
  
  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
  }
  data "aws_ami" "ubuntu" {
  most_recent = true
  
  filter {
  name   = "name"
  values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  
  filter {
  name   = "virtualization-type"
  values = ["hvm"]
  }
  
  owners = ["099720109477"] # Canonical
  }

  resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  
  tags = {
  Name = "HelloWorld"
  }
  }